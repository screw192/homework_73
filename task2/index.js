const express = require("express");
const app = express();
const port = 8000;

const Vigenere = require('caesar-salad').Vigenere;
const encryptionKey = "swordfish";

app.get("/encode/:text", (req, res) => {
  const encryptedString = Vigenere.Cipher(encryptionKey).crypt(req.params.text);

  res.send(encryptedString);
});

app.get("/decode/:text", (req, res) => {
  const decryptedString = Vigenere.Decipher(encryptionKey).crypt(req.params.text);

  res.send(decryptedString);
});

app.listen(port, () => {
  console.log("Listening on port: " + port);
});